# Temporal dynamics of marine protists communities in tidally mixed coastal waters

Here, we investigated the **annual successions of eukaryotic protists** at the SOMLIT-Astan time-series station (Roscoff, Western English Channel) by combining two biodiversity datasets.

- **morphological taxa counts**, 
- **genetic sequence reads** of Operational Taxonomic Units (OTUs) from a metabarcoding analysis (**V4 Illumina sequencing of the nuclear 18S rDNA**).  

The joint datasets allow to recover the absolute species abundance, while improving taxonomic resolution simultaneously.

Using a modelling approach and some distance based Moran’s eigenvector maps (dbMEM) in an eigenfunctions analysis developed for multiscale exploration, our results elucidate the **recurrent seasonal pattern of the dominant species and OTUs driving the succession**. 
In addition, the results of our models shows that the community is characterized by a seasonal: (i) winter-summer, (ii) spring-autumn temporal structure which could be driven by environmental forcing and a (iii) bi-annual one, which suggests that auto-organization processes of the community itself may be important for the resilience of this environment. 