library(tidyverse)
library(vegan)
library(data.table)
library(gridExtra)

library(ade4)
library(adespatial)
library(spdep)
library(ape)

setwd("~/PhD/1st paper PhD-Mary")

#### Import data ---------------------------------------------------
# Env data
env_data <- fread("tables_paper/envpar_wide.txt")
# Biological MetaB data
data_metaB <- fread("data/ASTAN_18SV4_407_samples_v202009.OTU.filtered.table.subseted")
# Remove what you don't need to keep just protists
data_metaB <- data_metaB[!grepl("Metazoa|Rhodophyta|Phaeophyceae|Ulvophyceae|Streptophyta", taxonomy) & identity>80]
# SPECIES table
data_morpho <- fread("data/MorphoDATA 2007-2017.txt", header=FALSE) 

#### Sort and trasform tables -------------------------------------
#MORPHO ## Community matrix 
# Transpose table
data_traspo <- data_morpho %>%
  t 
colnames(data_traspo) <- data_traspo[1,]
data_traspo <- data_traspo[-1,]
data_traspo <- as.data.table(data_traspo)
count_morpho <- subset(data_traspo, select = -c(Regroupement,AphiaID,Kingdom,Phylum,Class,Order,Family,Genus,Species,`Scientific_name`)) 
count_morpho <- count_morpho %>% select(Expert_entry, `07/01/2009`:`22/12/2016`) #199 samples
count_morpho <- count_morpho %>% dplyr::select(-`07/07/2014`,-`07/05/2014`)  #146 species and 197 samples
count_morpho <- subset(count_morpho, select = -c(`10/05/2010`,`15/11/2010`,`04/05/2011`,`20/05/2011`,`09/06/2011`,`05/09/2011`,  `16/04/2012`,`25/09/2012`, `19/04/2013`,`05/08/2014` ,`15/12/2014`,`10/08/2015`))

# I eliminate this two samples because I had to eliminate in metaB data
desc_morpho <- data_traspo[,list(AphiaID,Kingdom,Phylum,Class,Order,Family,Genus,Species,`Scientific_name`,`Expert_entry`)]
#
transp_morpho <- data.frame(count_morpho, row.names = "Expert_entry",check.names = F) %>%
  t %>%
  apply(.,c(1,2),as.numeric)
species_hel <- decostand(transp_morpho, method = "hellinger") %>% apply(.,c(1,2),as.numeric)
spe_final <- species_hel 

##METAB
# Create table for counting
count_mb <- data_metaB %>% select(amplicon, RA090107_02:RA161222_3) #16518 otus
desc_mb <- data_metaB[,list(amplicon,sequence,taxonomy,references,identity)]

desc_mb[grepl("Dinoflagellata|Ochrophyta",taxonomy),taxogroup:=sub("^([^\\|]+\\|){3}([^\\|]+).*$","\\2",taxonomy)]
desc_mb[!grepl("Dinoflagellata|Ochrophyta",taxonomy),taxogroup:=sub("^([^\\|]+\\|){2}([^\\|]+).*$","\\2",taxonomy)]
desc_mb[grepl("Eukaryota\\|",taxogroup),taxogroup:=sub("^.+\\|","",taxogroup)]
desc_mb[is.na(taxogroup),taxogroup:="Unassigned"]
## Merge tables and obtain final table ## 
final_mb <- merge(desc_mb,count_mb, by = "amplicon")
head(final_mb)

# Fraction 3
sple_3 <- colnames(final_mb) %>%
  grep("RA.+_3",.,value=T)
data_mat_3 <- final_mb[,.SD,.SDcols=c("amplicon",sple_3)] %>%
  data.frame(row.names = "amplicon")                            #Tot 16518 OTUs   and 187 samples
data_mat_3 <- data_mat_3[rowSums(data_mat_3)>0,]               #15205 otus and 187 samples
setDT(data_mat_3, keep.rownames = TRUE)[]  #Rownames to column
colnames(data_mat_3)[1] <- "amplicon"
data_final_3 <- merge(desc_mb, data_mat_3, by = "amplicon")
final_mb <- merge(desc_mb, data_mat_3, by = "amplicon")

#I eliminate the following samples because they need to be re-sequenced as the number of seq. is not enough
count_mb <- data_mat_3 %>% dplyr::select(-RA140707_3, -RA140507_3)   #15205 OTUs and 185 samples

# I take directly the RDA results
fauna.mem.pos.sel_mo <- readRDS("tables_paper/Morpho_RDAspemem.RDS")
fauna.mem.pos.sel_mb <- readRDS("tables_paper/MetaB_RDAotumem.RDS")

#Redo table OTU
transp_metaB <- data.frame(count_mb, row.names = "amplicon",check.names = F) %>%
  t %>%  apply(.,c(1,2),as.numeric)%>%
  .[,apply(.,2,function(X) sum(X>0)>10)] 
otu_final <- decostand(transp_metaB, method = "total")

#MORPHO
## Axes 1 ------------------------------------
target.1 <- summary(fauna.mem.pos.sel_mo)$constraint[,1] 
#fauna.mem.pos.sel is the rda between OTUs and the MEM positive selected
# target.1 are just the score of the RDA 1 (axes 1)
summary(target.1)
# target.1x <- as.data.frame(target.1)
# target.1x <- target.1x[row.names(target.1x) %in% rownames(env_par_final),] 
## Axes 2 ------------------------------------
target.2 <- summary(fauna.mem.pos.sel_mo)$constraint[,2]
## Axes 3 ------------------------------------
target.3 <- summary(fauna.mem.pos.sel_mo)$constraint[,3]

# target.4 <- summary(fauna.mem.pos.sel_mo)$constraint[,4]
# target.5 <- summary(fauna.mem.pos.sel_mo)$constraint[,5]
# target.6 <- summary(fauna.mem.pos.sel_mo)$constraint[,6]
target <- data.frame(target.1,target.2,target.3)
colnames(target) <- c("RDA 1", "RDA 2", "RDA 3")

#### Prepare environmental data needed ---------------------------------------
env_par <- env_data
# Add "sample" column
env_par[,sample:=sub("\\d{2}(\\d{2})-(\\d{2})-(\\d{2})","\\3/\\2/20\\1",sample)]
# Create new table and keep only sample present also in the otu table
#env_par <- env_par[sample%in%rownames(spe_final)]
# Put sample as row names
env_par <- data.frame(env_par,row.names="sample", check.names = F)
head(env_par)
# Remove the line with NA
env_par_final <- env_par
env_par_final <- env_par_final[rownames(env_par_final)%in%row.names(target),]
env_par_final <- env_par %>% drop_na()
target <- target[rownames(target)%in%row.names(env_par_final),]
env_par_final <- env_par_final[rownames(env_par_final)%in%row.names(target),]
## CORRELATIONS --------------------------------------------
library(corrplot)
x <- target.1
envpar <- env_par_final
y <- envpar[1:ncol(envpar)]
cor(x, y, use="complete.obs")

#same

cor.P <- cor(target, y= envpar, method = c("pearson"))
absval.P <-abs(cor.P)
png("figures_paper/Morpho_CorrelationPearson_ax1-2-3.png",width=8,height=6,units='in',res=400)
corrplot(cor.P, method="circle")
dev.off()
corrplot(cor.P, method="number")

cor.S <- cor(target, y= envpar, method = c("spearman"))
absval.S <-abs(cor.S)
png("figures&tables/MEMs_Morpho/Morpho_CorrelationSpearman_ax1-2-3.png",width=8,height=6,units='in',res=400)
corrplot(cor.S, method="circle")
dev.off()

cor.K <- cor(target, y= envpar, method = c("kendall"))
absval.K <-abs(cor.K)
png("figures&tables/MEMs_Morpho/Morpho_CorrelationKendall_ax1-2-3.png",width=8,height=6,units='in',res=400)
corrplot(cor.K, method="circle")
dev.off()

#METAB
## Axes 1 ------------------------------------
target.1 <- summary(fauna.mem.pos.sel_mb)$constraint[,1] 
#fauna.mem.pos.sel is the rda between OTUs and the MEM positive selected
# target.1 are just the score of the RDA 1 (axes 1)
summary(target.1)
# target.1x <- as.data.frame(target.1)
# target.1x <- target.1x[row.names(target.1x) %in% rownames(env_par_final),] 
## Axes 2 ------------------------------------
target.2 <- summary(fauna.mem.pos.sel_mb)$constraint[,2]
## Axes 3 ------------------------------------
target.3 <- summary(fauna.mem.pos.sel_mb)$constraint[,3]

target <- data.frame(target.1,target.2,target.3)
colnames(target) <- c("RDA 1", "RDA 2", "RDA 3")

#### Prepare environmental data needed ---------------------------------------
env_par <- env_data
# Add "sample" column
env_par[,samplename:=sub("(\\d{2})/(\\d{2})/\\d{2}(\\d{2})","RA\\3\\2\\1_3",sample)]
# Create new table and keep only sample present also in the otu table
#env_par <- env_par[sample%in%rownames(spe_final)]
# Put sample as row names
env_par <- data.frame(env_par,row.names="sample", check.names = F)
head(env_par)
# Remove the line with NA
env_par_final <- env_par
env_par_final <- env_par_final[env_par_final$samplename%in%row.names(target),]
env_par_final <- env_par %>% drop_na()
target <- target[rownames(target)%in%env_par_final$samplename,]
env_par_final <- env_par_final[env_par_final$samplename%in%row.names(target),]
env_par_final$samplename <- NULL
## CORRELATIONS --------------------------------------------
library(corrplot)
x <- target.1
envpar <- env_par_final
y <- envpar[1:ncol(envpar)]
cor(x, y, use="complete.obs")

#same

cor.P <- cor(target, y= envpar, method = c("pearson"))
absval.P <-abs(cor.P)
png("figures_paper/MetaB_CorrelationPearson_ax1-2-3.png",width=8,height=6,units='in',res=400)
corrplot(cor.P, method="circle")
dev.off()
corrplot(cor.P, method="number")

cor.S <- cor(target, y= envpar, method = c("spearman"))
absval.S <-abs(cor.S)
png("figures&tables/MEMs_MetaB/MetaB_CorrelationSpearman_ax1-2-3.png",width=8,height=6,units='in',res=400)
corrplot(cor.S, method="circle")
dev.off()

cor.K <- cor(target, y= envpar, method = c("kendall"))
absval.K <-abs(cor.K)
png("figures&tables/MEMs_MetaB/MetaB_CorrelationKendall_ax1-2-3.png",width=8,height=6,units='in',res=400)
corrplot(cor.K, method="circle")
dev.off()
# 
# ## Correlation test -------------------------------------------
# cor.test.P <- data.frame(matrix(ncol = 6, nrow = ncol(env_par_final)))
# colnames(cor.test.P) <- c("corr_coeff1", "pvalue1","corr_coeff2", "pvalue2","corr_coeff3", "pvalue3")
# 
# 
# for (i in 1:ncol(target)){
#   for(j in 1:ncol(env_par_final)){
#     corr <- cor.test(env_par_final[,j], target[,i], method = "pearson")
#     cor.test.P [i,1] <- corr$estimate
#     cor.test.P [i,2] <- corr$p.value
#     cor.test.P [i,3] <- corr$estimate
#     cor.test.P [i,4] <- corr$p.value
#     cor.test.P [i,5] <- corr$estimate
#     cor.test.P [i,6] <- corr$p.value
#   }
# }
# 
# 
# #Pearson
# cor.test1P <- data.frame(matrix(ncol = 2, nrow = ncol(env_par_final)))
# colnames(cor.test1P) <- c("corr_coefficient", "pvalue")
# 
# for(i in 1:ncol(env_par_final)){
#   corr <- cor.test(env_par_final[,i], target.1, method = "pearson")
#   cor.test1P[i,1] <- corr$estimate
#   cor.test1P[i,2] <- corr$p.value
# }
# 
# cor.test2P <- data.frame(matrix(ncol = 2, nrow = ncol(env_par_final)))
# colnames(cor.test2P) <- c("corr.coefficient", "pvalue")
# 
# for(i in 1:ncol(env_par_final)){
#   corr <- cor.test(env_par_final[,i], target.2, method = "pearson")
#   cor.test2P[i,1] <- corr$estimate
#   cor.test2P[i,2] <- corr$p.value
# }
# 
# cor.test3P  <- data.frame(matrix(ncol = 2, nrow = ncol(env_par_final)))
# colnames(cor.test3P) <- c("corr.coefficient", "pvalue")
# 
# for(i in 1:ncol(env_par_final)){
#   corr <- cor.test(env_par_final[,i], target.3, method = "pearson")
#   cor.test3P[i,1] <- corr$estimate
#   cor.test3P[i,2] <- corr$p.value
# }
# 
# #Spearman
# cor.test1S <- data.frame(matrix(ncol = 2, nrow = ncol(env_par_final)))
# colnames(cor.test1S) <- c("corr_coefficient", "pvalue")
# 
# for(i in 1:ncol(env_par_final)){
#   corr <- cor.test(env_par_final[,i], target.1, method = "spearman")
#   cor.test1S[i,1] <- corr$estimate
#   cor.test1S[i,2] <- corr$p.value
# }
# 
# cor.test2S <- data.frame(matrix(ncol = 2, nrow = ncol(env_par_final)))
# colnames(cor.test2S) <- c("corr.coefficient", "pvalue")
# 
# for(i in 1:ncol(env_par_final)){
#   corr <- cor.test(env_par_final[,i], target.2, method = "spearman")
#   cor.test2S[i,1] <- corr$estimate
#   cor.test2S[i,2] <- corr$p.value
# }
# 
# cor.test3S  <- data.frame(matrix(ncol = 2, nrow = ncol(env_par_final)))
# colnames(cor.test3S) <- c("corr.coefficient", "pvalue")
# 
# for(i in 1:ncol(env_par_final)){
#   corr <- cor.test(env_par_final[,i], target.3, method = "spearman")
#   cor.test3S[i,1] <- corr$estimate
#   cor.test3S[i,2] <- corr$p.value
# }
# 
# # Unify tables
# Pearson_cor.test  <- data.frame(cor.test1P,cor.test2P,cor.test3P)
# colnames(Pearson_cor.test) <- c("corr.coeff_RDA1", "p.value_RDA1","corr.coeff_RDA2", "p.value_RDA2","corr.coeff_RDA3", "p.value_RDA3")
# rownames(Pearson_cor.test) <- c(colnames(env_par_final))
# write.table(Pearson_cor.test, "Important info/Morpho_CorrelationTest_Pearson.txt")
# 
# Spearman_cor.test <- data.frame(cor.test1S,cor.test2S,cor.test3S)
# colnames(Spearman_cor.test) <- c("corr.coeff_RDA1", "p.value_RDA1","corr.coeff_RDA2", "p.value_RDA2","corr.coeff_RDA3", "p.value_RDA3")
# rownames(Spearman_cor.test) <- c(colnames(env_par_final))
# write.table(Spearman_cor.test, "Important info/Morpho_CorrelationTest_Spearman.txt")
