library(tidyverse)
library(vegan)
library(data.table)
library(gridExtra)

library(ade4)
library(adespatial)
library(spdep)
library(ape)

setwd("~/PhD/1st paper PhD-Mary")


#### Import data ---------------------------------------------------
# Biological MetaB data
data_metaB <- fread("data/ASTAN_18SV4_407_samples_v202009.OTU.filtered.table.subseted")
# Remove what you don't need to keep just protists
data_metaB <- data_metaB[!grepl("Metazoa|Rhodophyta|Phaeophyceae|Ulvophyceae|Streptophyta", taxonomy) & identity>80]
# SPECIES table
data_morpho <- fread("data/MorphoDATA 2007-2017.txt", header=FALSE) 

#### Sort and trasform tables -------------------------------------
#MORPHO ## Community matrix 
# Transpose table
data_traspo <- data_morpho %>%
  t 
colnames(data_traspo) <- data_traspo[1,]
data_traspo <- data_traspo[-1,]
data_traspo <- as.data.table(data_traspo)
count_morpho <- subset(data_traspo, select = -c(Regroupement,AphiaID,Kingdom,Phylum,Class,Order,Family,Genus,Species,`Scientific_name`)) 
count_morpho <- count_morpho %>% select(Expert_entry, `07/01/2009`:`22/12/2016`) #199 samples
count_morpho <- count_morpho %>% dplyr::select(-`07/07/2014`,-`07/05/2014`)  #146 species and 197 samples
count_morpho <- subset(count_morpho, select = -c(`10/05/2010`,`15/11/2010`,`04/05/2011`,`20/05/2011`,`09/06/2011`,`05/09/2011`,  `16/04/2012`,`25/09/2012`, `19/04/2013`,`05/08/2014` ,`15/12/2014`,`10/08/2015`))

# I eliminate this two samples because I had to eliminate in metaB data
desc_morpho <- data_traspo[,list(AphiaID,Kingdom,Phylum,Class,Order,Family,Genus,Species,`Scientific_name`,`Expert_entry`)]
#
transp_morpho <- data.frame(count_morpho, row.names = "Expert_entry",check.names = F) %>%
  t %>%
  apply(.,c(1,2),as.numeric)
species_hel <- decostand(transp_morpho, method = "total") %>% apply(.,c(1,2),as.numeric)
spe_final <- species_hel 

##METAB
# Create table for counting
count_mb <- data_metaB %>% select(amplicon, RA090107_02:RA161222_3) #16518 otus
desc_mb <- data_metaB[,list(amplicon,sequence,taxonomy,references,identity)]

desc_mb[grepl("Dinoflagellata|Ochrophyta",taxonomy),taxogroup:=sub("^([^\\|]+\\|){3}([^\\|]+).*$","\\2",taxonomy)]
desc_mb[!grepl("Dinoflagellata|Ochrophyta",taxonomy),taxogroup:=sub("^([^\\|]+\\|){2}([^\\|]+).*$","\\2",taxonomy)]
desc_mb[grepl("Eukaryota\\|",taxogroup),taxogroup:=sub("^.+\\|","",taxogroup)]
desc_mb[is.na(taxogroup),taxogroup:="Unassigned"]
## Merge tables and obtain final table ## 
final_mb <- merge(desc_mb,count_mb, by = "amplicon")
head(final_mb)

# Fraction 3
sple_3 <- colnames(final_mb) %>%
  grep("RA.+_3",.,value=T)
data_mat_3 <- final_mb[,.SD,.SDcols=c("amplicon",sple_3)] %>%
  data.frame(row.names = "amplicon")                            #Tot 16518 OTUs   and 187 samples
data_mat_3 <- data_mat_3[rowSums(data_mat_3)>0,]               #15205 otus and 187 samples
setDT(data_mat_3, keep.rownames = TRUE)[]  #Rownames to column
colnames(data_mat_3)[1] <- "amplicon"
data_final_3 <- merge(desc_mb, data_mat_3, by = "amplicon")
final_mb <- merge(desc_mb, data_mat_3, by = "amplicon")

#I eliminate the following samples because they need to be re-sequenced as the number of seq. is not enough
count_mb <- data_mat_3 %>% dplyr::select(-RA140707_3, -RA140507_3)   #15205 OTUs and 185 samples

# I take directly the RDA results
fauna.mem.pos.sel_mo <- readRDS("tables_paper/Morpho_RDAspemem.RDS")
fauna.mem.pos.sel_mb <- readRDS("tables_paper/MetaB_RDAotumem.RDS")

#Redo table OTU
transp_metaB <- data.frame(count_mb, row.names = "amplicon",check.names = F) %>%
  t %>%  apply(.,c(1,2),as.numeric)%>%
  .[,apply(.,2,function(X) sum(X>0)>10)] 
otu_final <- decostand(transp_metaB, method = "total")

## MORPHO --------------------------
#### Plot with absolute scores of morpho species 
#Select axes 1
score_1_abs <- scores(fauna.mem.pos.sel_mo, scaling = 1)$species
score_1_abs <- score_1_abs[order(abs(score_1_abs[,1]), decreasing = T),]
SPE_group1_abs <- spe_final[,colnames(spe_final)%in%row.names(score_1_abs)[1:20]] %>%
  melt %>% data.table
SPE_group1_abs <- SPE_group1_abs[,date:=gsub("^_/_/_","",Var1) %>% as.Date(.,"%d/%m/%Y")] %>% separate(date, sep = "-", into = c("year", "month", "day"), remove = FALSE)
colnames(SPE_group1_abs) <- c("sample","taxo","value","date","year","month","day") 

# Add column with name of species
spec <- as.data.table(desc_morpho, keep.rownames=TRUE)
spec[,taxo:=sub("^._$","",Expert_entry)]
spec[is.na(taxo),taxo:="Unassigned"]

SPE_group1_abs <- merge(SPE_group1_abs, spec, by="taxo")
SPE_group1_abs[,taxo:=paste(SPE_group1_abs$Class,"-",SPE_group1_abs$taxo)]
SPE_group1_abs <- SPE_group1_abs[,axes:= 1]

# png("figures_paperEXTRA/Morpho20scoreRDA1.png",width=16,height=8,units='in',res=400)
# ggplot(SPE_group1_abs, aes(x=date, y=value,group=taxo)) +
#   geom_point()+
#   geom_line()+
#   ggtitle("(RDA Axes 1)") +
#   xlab("date") +
#   ylab("Species abundance")+
#   facet_wrap(~taxo,ncol=1,scales = "free_y")
# dev.off()

#Select axes 2
score_2_abs <- scores(fauna.mem.pos.sel_mo, scaling = 1)$species
score_2_abs <- score_2_abs[order(abs(score_2_abs[,2]), decreasing = T),]

SPE_group2_abs <- spe_final[,colnames(spe_final)%in%row.names(score_2_abs)[1:20]] %>%
  melt %>% data.table
SPE_group2_abs <- SPE_group2_abs[,date:=gsub("^_/_/_","",Var1) %>% as.Date(.,"%d/%m/%Y")] %>% separate(date, sep = "-", into = c("year", "month", "day"), remove = FALSE)
colnames(SPE_group2_abs) <- c("sample","taxo","value","date","year","month","day") 
SPE_group2_abs <- merge(SPE_group2_abs, spec, by="taxo")
SPE_group2_abs[,taxo:=paste(SPE_group2_abs$Class,"-",SPE_group2_abs$taxo)]
SPE_group2_abs <- SPE_group2_abs[,axes:= 2]

# png("figures_paperEXTRA/Morpho20scoreRDA2.png",width=16,height=8,units='in',res=400)
# ggplot(SPE_group2_abs, aes(x=date, y=value,group=taxo)) +
#   geom_point()+
#   geom_line()+
#   ggtitle("(RDA Axes 2)") +
#   xlab("date") +
#   ylab("Species abundance")+
#   facet_wrap(~taxo,ncol=1,scales = "free_y")
# dev.off()

## HEATMAP MORPHO ----------------------------------
# library(hrbrthemes)
# library(plotly)

final_1 <- SPE_group1_abs[,sum(value),by=list(taxo,date,Class,Expert_entry,year,month,axes)]
final_1 <- final_1[,mean(V1),by=list(month,year,Class,Expert_entry,axes)]
final_1 <- final_1[,V1:=V1/max(V1),by="Expert_entry"][order(V1, decreasing = T)]
setnames(final_1, "V1", "value")
final_2 <- SPE_group2_abs[,sum(value),by=list(taxo,date,Class,Expert_entry,year,month,axes)]
final_2 <- final_2[,mean(V1),by=list(month,year,Class,Expert_entry,axes)]
final_2 <- final_2[,V1:=V1/max(V1),by="Expert_entry"][order(V1, decreasing = T)]
setnames(final_2, "V1", "value")

# p1= ggplot(final_1, aes(x=month, y=Expert_entry, fill= Class, alpha=value)) + 
#   geom_tile() +
#   xlab("time") +
#   ylab("Species_axes1") +
#   scale_fill_manual(values = c(Bacillariophyceae = '#2171b5',Dinophyceae = '#FF6600'))+
#   theme_bw()+
#   theme(panel.spacing = unit(0, "lines"),legend.position = "top",legend.text = element_text(size=14), legend.title = element_blank(), 
#         axis.text.x = element_text(angle = 0, hjust = 0.55, vjust = 0.55, size = 8),
#         axis.title = element_text(size=16),
#         plot.title = element_text(size=28))+
# facet_grid(~year)
# 
# p2= ggplot(final_2, aes(x=month, y=Expert_entry, fill= Class, alpha=value)) + 
#   geom_tile() +
#   xlab("time") +
#   ylab("Species_axes2") +
#   scale_fill_manual(values = c(Bacillariophyceae = '#2171b5',Dinophyceae = '#FF6600', Oligotrichea='#FF3399', Prymnesiophyceae='#33FF33'))+
#   theme_bw()+
#   theme(panel.spacing = unit(0, "lines"),legend.position = "top",legend.text = element_text(size=14), legend.title = element_blank(), 
#         axis.text.x = element_text(angle = 0, hjust = 0.55, vjust = 0.55, size = 8),
#         axis.title = element_text(size=16),
#         plot.title = element_text(size=28))+
# facet_grid(~year)
# 
# png("figures_paper/MorphoSpecies20SCORE_3axes_Fig.png",,width=16,height=8,units='in',res=400)
# grid.arrange(p1,p2,nrow=3)
# dev.off()

##Unique HEATMAP_First figure
SPE_allgroups <- rbind(final_1,final_2)

png("figures_paper/Morpho20score_2axes.png",width=16,height=8,units='in',res=400)
ggplot(SPE_allgroups, aes(x=month, y=Expert_entry, fill= Class, alpha=value)) + 
  geom_tile() +
  xlab("time") +
  ylab("OTUs_axes3") +
  scale_fill_manual(values = c(Bacillariophyceae = '#2171b5',Dinophyceae = '#FF6600', Oligotrichea='#FF3399', Prymnesiophyceae='#33FF33'))+
  theme_bw()+
  theme(panel.spacing = unit(0, "lines"),legend.position = "top",legend.text = element_text(size=14), legend.title = element_blank(), 
        axis.text.x = element_text(angle = 0, hjust = 0.55, vjust = 0.55, size = 8),
        axis.title = element_text(size=16),
        plot.title = element_text(size=28))+
  facet_grid(axes~year, scales = "free")
dev.off()

##METAB --------------------------------
#### Plot with absolute scores of OTUs metaB
#Select axes 1
score_1_abs <- scores(fauna.mem.pos.sel_mb, scaling = 1)$species
score_1_abs <- score_1_abs[order(abs(score_1_abs[,1]), decreasing = T),]
OTU_group1_abs <- otu_final[,colnames(otu_final)%in%row.names(score_1_abs)[1:20]] %>%
  melt %>% data.table
OTU_group1_abs <- OTU_group1_abs[,date:=gsub("^RA|_3","",Var1) %>% as.Date(.,"%y%m%d")] %>% separate(date, sep = "-", into = c("year", "month", "day"), remove = FALSE)
colnames(OTU_group1_abs) <- c("sample","amplicon","value","date","year","month", "day")

# Add column with name of species
spec <- as.data.table(desc_mb, keep.rownames=TRUE)
spec[,taxospecies:=sub("^.+\\|","",taxonomy)]
spec[is.na(taxospecies),taxospecies:="Unassigned"]

OTU_group1_abs <- merge(OTU_group1_abs, spec, by="amplicon")
OTU_group1_abs[,taxo:=paste(OTU_group1_abs$taxogroup,"-",OTU_group1_abs$taxospecies)]
OTU_group1_abs[,key:=paste(taxospecies,sub("^(.{4}).+$","\\1",amplicon))]
OTU_group1_abs <- OTU_group1_abs[,axes:= 1]

# png("figures_paperEXTRA/MetaB20scoreRDA1.png",width=16,height=8,units='in',res=400)
# ggplot(OTU_group1_abs, aes(x=date, y=value,group=taxo)) +
#   geom_point()+
#   geom_line()+
#   ggtitle("Summer-Winter OTUs (RDA Axes 1)") +
#   xlab("date") +
#   ylab("Relative abundance")+
#   facet_wrap(~key,ncol=1,scales = "free_y")
# dev.off()

#Select axes 2
score_2_abs <- scores(fauna.mem.pos.sel_mb, scaling = 1)$species
score_2_abs <- score_2_abs[order(abs(score_2_abs[,2]), decreasing = T),]
OTU_group2_abs <- otu_final[,colnames(otu_final)%in%row.names(score_2_abs)[1:20]] %>%
  melt %>% data.table
OTU_group2_abs <- OTU_group2_abs[,date:=gsub("^RA|_3","",Var1) %>% as.Date(.,"%y%m%d")] %>% separate(date, sep = "-", into = c("year", "month", "day"), remove = FALSE)
colnames(OTU_group2_abs) <- c("sample","amplicon","value","date","year","month", "day")
OTU_group2_abs <- merge(OTU_group2_abs, spec, by="amplicon")
OTU_group2_abs[,taxo:=paste(OTU_group2_abs$taxogroup,"-",OTU_group2_abs$taxospecies)]
OTU_group2_abs[,key:=paste(taxospecies,sub("^(.{4}).+$","\\1",amplicon))]
OTU_group2_abs <- OTU_group2_abs[,axes:= 2]

# png("figures_paperEXTRA/MetaB20scoreRDA2.png",width=16,height=8,units='in',res=400)
# ggplot(OTU_group2_abs, aes(x=date, y=value,group=taxo)) +
#   geom_point()+
#   geom_line()+
#   ggtitle("Spring-Autumn OTUs (RDA Axes 2)") +
#   xlab("date") +
#   ylab("Relative abundance")+
#   facet_wrap(~key,ncol=1,scales = "free_y")
# dev.off()
## HEATMAP METAB ----------------------------------
final_1 <- OTU_group1_abs[,sum(value),by=list(key,date,taxogroup,taxospecies,year,month,axes)]
final_1 <- final_1[,mean(V1),by=list(month,year,taxogroup,taxospecies,key,axes)]
final_1 <- final_1[,V1:=V1/max(V1),by="key"][order(V1, decreasing = T)]
setnames(final_1, "V1", "value")
final_2 <- OTU_group2_abs[,sum(value),by=list(key,date,taxogroup,taxospecies,year,month,axes)]
final_2 <- final_2[,mean(V1),by=list(month,year,taxogroup,taxospecies,key,axes)]
final_2 <- final_2[,V1:=V1/max(V1),by="key"][order(V1, decreasing = T)]
setnames(final_2, "V1", "value")

# 
# p1= ggplot(final_1, aes(x=month, y=key, fill= taxogroup, alpha=value)) +
#   geom_tile() +
#   xlab("time") +
#   ylab("OTUs_axes1") +
#   scale_fill_manual(values = c(Bacillariophyta = '#2171b5', Cercozoa = 'gold', Chlorophyta = '#00CCFF', Cryptophyta = '#66CC00', Dinoflagellata ='Ivory1', Dinophyceae = '#FF6600', Syndiniales = '#bd0026', Picozoa = '#CC3399', Pseudofungi = 'coral4'))+
#   theme_bw()+
#   theme(panel.spacing = unit(0, "lines"),legend.position = "top",legend.text = element_text(size=14), legend.title = element_blank(),
#         axis.text.x = element_text(angle = 0, hjust = 0.55, vjust = 0.55, size = 8),
#         axis.title = element_text(size=16),
#         plot.title = element_text(size=28))+
#   facet_grid(~year)
# 
# p2= ggplot(final_2, aes(x=month, y=key, fill= taxogroup, alpha=value)) +
#   geom_tile() +
#   xlab("time") +
#   ylab("OTUs_axes2") +
#   scale_fill_manual(values = c(Bacillariophyta = '#2171b5', Cercozoa = 'gold', Chlorophyta = '#00CCFF', Cryptophyta = '#66CC00', Dinoflagellata ='Ivory1', Dinophyceae = '#FF6600', Syndiniales = '#bd0026', Picozoa = '#CC3399', Pseudofungi = 'coral4'))+
#   theme_bw()+
#   theme(panel.spacing = unit(0, "lines"),legend.position = "top",legend.text = element_text(size=14), legend.title = element_blank(),
#         axis.text.x = element_text(angle = 0, hjust = 0.55, vjust = 0.55, size = 8),
#         axis.title = element_text(size=16),
#         plot.title = element_text(size=28))+
#   facet_grid(~year)
# 
# png("figures_paper/MetaBOTUs20_scoreRDAaxes_Fig.png",,width=18,height=10,units='in',res=400)
# grid.arrange(p1,p2,nrow=3)
# dev.off()

OTU_allgroups <- rbind(final_1,final_2)

png("figures_paper/MetaB_20score_2axes.png",,width=16,height=8,units='in',res=400)
ggplot(OTU_allgroups, aes(x=month, y=key, fill= taxogroup, alpha=value)) + 
  geom_tile() +
  xlab("time") +
  ylab("OTUs_axes3") +
  scale_fill_manual(values = c(Bacillariophyta = '#2171b5', Cercozoa = 'gold', Chlorophyta = '#00CCFF', Cryptophyta = '#66CC00', Dinophyceae = '#FF6600', Syndiniales = '#bd0026', Picozoa = '#CC3399', Pseudofungi = 'coral4'))+
  theme_bw()+
  theme(panel.spacing = unit(0, "lines"),legend.position = "top",legend.text = element_text(size=14), legend.title = element_blank(), 
        axis.text.x = element_text(angle = 0, hjust = 0.55, vjust = 0.55, size = 8),
        axis.title = element_text(size=16),
        plot.title = element_text(size=28))+
  facet_grid(axes~year, scales= "free")
dev.off()
